package com.cursospring.bookstoremanager.controller;

import com.cursospring.bookstoremanager.dto.BookDTO;
import com.cursospring.bookstoremanager.dto.MessageResponseDTO;
import com.cursospring.bookstoremanager.exception.BookNotFoundException;
import com.cursospring.bookstoremanager.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/books")
public class BookController {

    @Autowired
    private BookService service;

    @PostMapping
    public MessageResponseDTO create(@RequestBody @Valid BookDTO bookDTO) {
        return service.create(bookDTO);
    }

    @GetMapping(value = "/{id}")
    public BookDTO findbyId(@PathVariable Long id) throws BookNotFoundException {
        return service.findById(id);
    }
}
