package com.cursospring.bookstoremanager.service;

import com.cursospring.bookstoremanager.dto.BookDTO;
import com.cursospring.bookstoremanager.dto.MessageResponseDTO;
import com.cursospring.bookstoremanager.entity.Book;
import com.cursospring.bookstoremanager.exception.BookNotFoundException;
import com.cursospring.bookstoremanager.mapper.BookMapper;
import com.cursospring.bookstoremanager.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookService {

    @Autowired
    private BookRepository repository;

    private final BookMapper bookMapper = BookMapper.INSTANCE;

    public MessageResponseDTO create(BookDTO bookDTO) {
        Book bookToSave = bookMapper.toModel(bookDTO);
        Book savedBook = repository.save(bookToSave);
        return MessageResponseDTO.builder().message("Livro salvo com o ID: " + savedBook.getId()).build();
    }

    public BookDTO findById(Long id) throws BookNotFoundException {
        Book obj = repository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
        return bookMapper.toDTO(obj);
    }
}
