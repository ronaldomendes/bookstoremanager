package com.cursospring.bookstoremanager.service;

import com.cursospring.bookstoremanager.dto.BookDTO;
import com.cursospring.bookstoremanager.entity.Book;
import com.cursospring.bookstoremanager.exception.BookNotFoundException;
import com.cursospring.bookstoremanager.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.cursospring.bookstoremanager.utils.BookUtils.createFakeBook;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    private BookRepository repository;

    @InjectMocks
    private BookService service;

    @Test
    void testP() {
        assertTrue(true);
    }

//    @Test
    void whenGivenExistingIdThenReturnThisBook() throws BookNotFoundException {
        Book book = createFakeBook();
        when(repository.findById(book.getId())).thenReturn(Optional.of(book));
        BookDTO bookDTO = service.findById(book.getId());

        assertEquals(book.getName(), bookDTO.getName());
        assertEquals(book.getIsbn(), bookDTO.getIsbn());
        assertEquals(book.getPublisherName(), bookDTO.getPublisherName());
    }

//    @Test
    void whenGivenUnexistingIdThenNotFindThrowAnException() {
        var invalidId = 10L;
        when(repository.findById(invalidId)).thenReturn(Optional.ofNullable(any(Book.class)));
        assertThrows(BookNotFoundException.class, () -> service.findById(invalidId));
    }
}
