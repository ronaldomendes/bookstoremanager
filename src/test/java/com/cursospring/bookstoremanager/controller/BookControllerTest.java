package com.cursospring.bookstoremanager.controller;

import com.cursospring.bookstoremanager.dto.BookDTO;
import com.cursospring.bookstoremanager.dto.MessageResponseDTO;
import com.cursospring.bookstoremanager.service.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import static com.cursospring.bookstoremanager.utils.BookUtils.asJsonString;
import static com.cursospring.bookstoremanager.utils.BookUtils.createFakeBookDTO;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class BookControllerTest {

    private static final String BOOK_API_URL_PATH = "/api/v1/books";

    private MockMvc mockMvc;

    @Mock
    private BookService service;

    @InjectMocks
    private BookController controller;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setViewResolvers((viewName, locale) -> new MappingJackson2JsonView()).build();
    }

    @Test
    void testP() {
        assertTrue(true);
    }

//    @Test
    void testWhenPOSTisCalledThenABookShouldBeCreated() throws Exception {
        BookDTO bookDTO = createFakeBookDTO();
        MessageResponseDTO msg = MessageResponseDTO.builder()
                .message("Livro salvo com o ID: " + bookDTO.getId()).build();

        when(service.create(bookDTO)).thenReturn(msg);
        mockMvc.perform(post(BOOK_API_URL_PATH).contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(bookDTO))).andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(msg.getMessage())));
    }

//    @Test
    void testWhenPOSTwithInvalidISBNIsCalledThenBadRequestShouldBeReturned() throws Exception {
        BookDTO bookDTO = createFakeBookDTO();
        bookDTO.setIsbn("invalid isbn");

        mockMvc.perform(post(BOOK_API_URL_PATH).contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(bookDTO))).andExpect(status().isBadRequest());
    }


}
